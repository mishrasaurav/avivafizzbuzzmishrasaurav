﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FuzzBizzAPI.Controllers;

namespace FuzzBizzAPI.Tests.Controllers
{
    [TestClass]
    public class FuzzBizzAPITest
    {
        [TestMethod]
        public void SaveNumber()
        {

            // Arrange
            FuzzBizzController controller = new FuzzBizzController();

            // Act
            controller.SaveNumber(5);
        }
    }
}
