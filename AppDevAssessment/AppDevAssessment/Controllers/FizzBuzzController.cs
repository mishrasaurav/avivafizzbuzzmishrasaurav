﻿using AppDevAssessment.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace AppDevAssessment.Controllers
{
    public class FizzBuzzController : Controller
    {
        [HttpPost]
        public ActionResult PersistEnteredNumber(NumberEntry model)
        {
            // call the WebApi To save the number
            int num=model.EnteredNumber;

            using (var client = new HttpClient())
            {
                var saveNumberUrl = Url.RouteUrl(
                    "DefaultApi",
                    new { httproute = "", controller = "fuzzBizz", id = num },
                    Request.Url.Scheme
                );
                var result = client
                            .GetAsync(saveNumberUrl)
                            .Result;
                return View(result);
            }
        }

    }
}
