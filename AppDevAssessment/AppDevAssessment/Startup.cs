﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AppDevAssessment.Startup))]
namespace AppDevAssessment
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
