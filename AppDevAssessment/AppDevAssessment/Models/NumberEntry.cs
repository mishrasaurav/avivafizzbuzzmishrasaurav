﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AppDevAssessment.Models
{
    public class NumberEntry
    {
        [Range(1,1000, ErrorMessage = "You need to enter a number between 1 to 1000")]
        public Int32 EnteredNumber { get; set; }
    }
}