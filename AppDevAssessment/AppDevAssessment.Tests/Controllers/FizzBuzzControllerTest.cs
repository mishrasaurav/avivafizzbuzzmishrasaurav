﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AppDevAssessment.Controllers;
using System.Web.Mvc;
using AppDevAssessment.Models;

namespace AppDevAssessment.Tests.Controllers
{
    [TestClass]
    public class FizzBuzzControllerTest
    {
        /// <summary>
        /// the controller method test
        /// </summary>
        [TestMethod]
        public void TestPersistEnteredNumber()
        {
            // Arrange
            FizzBuzzController controller = new FizzBuzzController();

            NumberEntry num = new NumberEntry();
            num.EnteredNumber = 76;

            // Act
            ViewResult result = controller.PersistEnteredNumber(num) as ViewResult;


            // Assert
            Assert.IsNotNull(result);
        }
    }
}
